//
//  Resolver.swift
//  ThirdHomeWorkApp
//
//  Created by Anton Agafonov on 12.07.2022.
//

import Foundation

class Resolver {
    private var container: [String: Any] = .init()
    
    func getDependency<T>(type: T.Type) -> T? {
        let key = String(describing: T.self)
        print("key", key)
        print("container", container)
        return container[key] as? T
    }
    
    func addDependency<T>(dependency: T) {
        let key = String(describing: T.self)
        container[key] = dependency
        print("key", key)
        print("container", container)
    }
}
