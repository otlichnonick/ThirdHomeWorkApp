//
//  Configurator.swift
//  ThirdHomeWorkApp
//
//  Created by Anton Agafonov on 13.07.2022.
//

import Foundation
import ServicesPackage

class Configurator {
    static let shared: Configurator = .init()
    let resolver: Resolver = .init()
    
    func setup() {
        registerServices()
    }
    
    private func registerServices() {
        resolver.addDependency(dependency: AuthService() as IAuthService)
        resolver.addDependency(dependency: StoreService() as IStoreService)
        resolver.addDependency(dependency: SettingsService() as ISettingsService)
        resolver.addDependency(dependency: KeychainService() as IKeychainService)
    }
}
