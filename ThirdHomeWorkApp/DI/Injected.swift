//
//  Injected.swift
//  ThirdHomeWorkApp
//
//  Created by Anton Agafonov on 12.07.2022.
//

import Foundation

@propertyWrapper
struct Injected<Service> {
    private var dependency: Service?
    private weak var resolver = Configurator.shared.resolver
    
    var wrappedValue: Service? {
        mutating get {
            if dependency == nil {
                self.dependency = resolver?.getDependency(type: Service.self)
                return dependency
            } else {
                return dependency
            }
        }
        mutating set {
            self.dependency = newValue
        }
    }
    
    var projectedValue: Injected<Service> {
        get {
            return self
        }
        set {
            self = newValue
        }
    }
}
