//
//  WelcomeScreen.swift
//  ThirdHomeWorkApp
//
//  Created by Anton Agafonov on 13.07.2022.
//

import SwiftUI

struct WelcomeScreen: View {
    @ObservedObject var viewModel: ViewModel
    
    var body: some View {
        VStack {
            Text("Добро пожаловать, \(viewModel.username)")
            
            Spacer()
        }
        .navigationBarTitleDisplayMode(.inline)
    }
}

struct WelcomeScreen_Previews: PreviewProvider {
    static var previews: some View {
        WelcomeScreen(viewModel: ViewModel())
    }
}
