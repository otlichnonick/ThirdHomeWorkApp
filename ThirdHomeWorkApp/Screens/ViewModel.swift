//
//  ViewModel.swift
//  ThirdHomeWorkApp
//
//  Created by Anton Agafonov on 13.07.2022.
//

import Combine
import UIKit
import ServicesPackage

class ViewModel: ObservableObject {
    @Injected var authService: IAuthService?
    @Injected var storeService: IStoreService?
    @Injected var settingsService: ISettingsService?
    @Injected var keychainService: IKeychainService?
    
    @Published var errorMessage: String = ""
    @Published var navLinkIsActive: Bool = false
    @Published var showActionSheet: Bool = false
    @Published var username: String = ""
    @Published var password: String = ""
    
    var theme: ColorThemes {
        settingsService?.theme ?? .regular
    }
    
    func login() {
        authService?.login(input: AuthModelRequest(login: username, password: password)) { [weak self] (result: Result<AuthModelResponse, Error>) in
//            switch result {
//            case .success(let success):
//                self?.keychainService?.set(accessToken: success.accessToken)
//                self?.keychainService?.set(refreshToken: success.refreshToken)
//            case .failure(let failure):
//                self?.errorMessage = failure.localizedDescription
//            }
        }
        navLinkIsActive = true
    }
    
    func set(newTheme: ColorThemes) {
        settingsService?.changeTheme(with: newTheme)
    }
}
