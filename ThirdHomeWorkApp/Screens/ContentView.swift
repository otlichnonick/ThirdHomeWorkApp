//
//  ContentView.swift
//  ThirdHomeWorkApp
//
//  Created by Anton Agafonov on 12.07.2022.
//

import SwiftUI

struct ContentView: View {
    @StateObject var viewModel: ViewModel = .init()
    
    var body: some View {
        NavigationView {
            VStack {
                TextField("Логин", text: $viewModel.username)
                    .textFieldStyle(RoundedBorderTextFieldStyle())
                
                TextField("Пароль", text: $viewModel.password)
                    .textFieldStyle(RoundedBorderTextFieldStyle())
                
                Button {
                    viewModel.login()
                } label: {
                    Text("Авторизоваться")
                        .font(.title)
                        .foregroundColor(Color(uiColor: viewModel.theme.textColor))
                        .background(Color(uiColor: viewModel.theme.textBackgroundColor))
                }
                .padding()
                
                Button {
                    viewModel.showActionSheet.toggle()
                } label: {
                    Text("Сменить цветовую тему")
                        .font(.title)
                        .foregroundColor(Color(uiColor: viewModel.theme.textColor))
                        .background(Color(uiColor: viewModel.theme.textBackgroundColor))
                }

                
                Spacer()
            }
            .padding()
            .background(
                NavigationLink(isActive: $viewModel.navLinkIsActive, destination: {
                    WelcomeScreen(viewModel: viewModel)
                }, label: {
                    EmptyView()
                })
            )
            .navigationBarHidden(true)
            .actionSheet(isPresented: $viewModel.showActionSheet) {
                ActionSheet(title: Text("Выберите цветовую схему"), buttons: [
                    .default(Text("Обычная"), action: {
                        viewModel.set(newTheme: .regular)
                    }),
                    .default(Text("Черно-белая"), action: {
                        viewModel.set(newTheme: .blackAndWhite)
                    }),
                    .default(Text("Сепия"), action: {
                        viewModel.set(newTheme: .sepia)
                    }),
                    .cancel()
                ])
            }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView(viewModel: ViewModel())
    }
}
