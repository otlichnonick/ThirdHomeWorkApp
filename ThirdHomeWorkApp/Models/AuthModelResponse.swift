//
//  AccessTokenModel.swift
//  ThirdHomeWorkApp
//
//  Created by Anton Agafonov on 13.07.2022.
//

import Foundation

struct AuthModelResponse: Codable {
    let accessToken: String
    let refreshToken: String
}
