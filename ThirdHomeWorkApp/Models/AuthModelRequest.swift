//
//  UserModel.swift
//  ThirdHomeWorkApp
//
//  Created by Anton Agafonov on 13.07.2022.
//

import Foundation

struct AuthModelRequest: Codable {
    let login: String
    let password: String
}
